package com.morrisons.wholesale.authorization.module;

import org.skife.jdbi.v2.DBI;

import com.google.inject.Provides;
import com.morrisons.wholesale.authorization.configuration.AuthorizationConfiguration;

import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class AuthorizationModule extends DropwizardAwareModule<AuthorizationConfiguration> {

	private DBI jdbi;

	@Provides
	public DBI prepareJdbi(Environment environment, AuthorizationConfiguration configuration) {

		if (jdbi == null) {
			final DBIFactory factory = new DBIFactory();
			jdbi = factory.build(environment, configuration.getDatabase(), "postgresql");
		}
		return jdbi;
	}

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		
	}

}
