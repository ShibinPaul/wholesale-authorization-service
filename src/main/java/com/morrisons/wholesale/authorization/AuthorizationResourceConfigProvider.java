package com.morrisons.wholesale.authorization;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.morrisons.wholesale.authorization.exception.AuthorizationServiceException;
import com.morrisons.wholesale.authorization.exception.ErrorCodes;

import io.dropwizard.configuration.ConfigurationSourceProvider;

public class AuthorizationResourceConfigProvider implements ConfigurationSourceProvider {

	private static final String ENV_KEY = "xxwmm_env";
	private static final Map<String, String> envSpecificConfig = new HashMap<>();
	static {
		envSpecificConfig.put("dev", "application_DEV.yml");
		envSpecificConfig.put("local", "application_LOCAL.yml");
		envSpecificConfig.put("cit", "application_CIT.yml");
		envSpecificConfig.put("sit", "application_SIT.yml");
		envSpecificConfig.put("uat", "application_UAT.yml");
		envSpecificConfig.put("preprod", "application_PREPROD.yml");
		envSpecificConfig.put("prod", "application_PROD.yml");
	}

	@Override
	public InputStream open(String path){
		if (envSpecificConfig.get(System.getenv(ENV_KEY)) != null) {
			final String envSpecificFileName = envSpecificConfig.get(System.getenv(ENV_KEY));
			return AuthorizationResourceConfigProvider.class.getClassLoader().getResourceAsStream(envSpecificFileName);
		} else {
			throw new AuthorizationServiceException(ErrorCodes.ENV_NOT_FOUND, "Environment not found");
		}
	}

}
