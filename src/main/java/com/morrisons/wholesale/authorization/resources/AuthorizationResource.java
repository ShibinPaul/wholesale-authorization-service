package com.morrisons.wholesale.authorization.resources;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.morrisons.wholesale.authorization.configuration.AuthorizationConfiguration;
import com.morrisons.wholesale.authorization.constant.Constants;
import com.morrisons.wholesale.authorization.exception.AuthorizationServiceException;
import com.morrisons.wholesale.authorization.exception.ErrorCodes;
import com.morrisons.wholesale.authorization.model.GroupUser;
import com.morrisons.wholesale.authorization.model.UserGroup;
import com.morrisons.wholesale.authorization.utils.FileUtils;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(value = "/v1")
@Api(value = "AuthorizationResource", description = "AuthorizationResource Service")
public class AuthorizationResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationResource.class);

	private static final String ACCESS_CONTROL_ALLOW_HEADERS = "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin";

	@Inject
	private AuthorizationConfiguration configuration;

	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Authorization Group User Details", response = GroupUser.class)
	@Path("/customers/{customerid}/groups/{groupid}/authorisation")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid specification"),
			@ApiResponse(code = 404, message = "Request not found") })
	public Response getGroupUserDetails(
			@ApiParam(value = "Customer ID", required = true) @PathParam("customerid") String customerId,
			@ApiParam(value = "Group ID", required = true) @PathParam("groupid") String groupId) {

		LOGGER.debug("AuthorizationResource getGroupUserDetails() STARTs");

		GroupUser groupUser;

		if (configuration.isTesting()) {
			try {
				groupUser =  new ObjectMapper().readValue(FileUtils.getFileAsStream(Constants.AUTHORIZATION_GROUP_USER_JSON),
						GroupUser.class);
				
				if(groupUser != null){
					return Response.status(HttpStatus.OK_200).entity(groupUser)
							.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
							.header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, ACCESS_CONTROL_ALLOW_HEADERS).build();
				}
			} catch (IOException e) {
				String msg = "Group User JSON not found/has error";
				AuthorizationServiceException pe = new AuthorizationServiceException(ErrorCodes.NO_RECORDS_FOUND, msg,
						e);
				LOGGER.error(msg, pe);
				throw pe;
			}
		}
		LOGGER.debug("AuthorizationResource getGroupUserDetails() ENDs");
		throw new AuthorizationServiceException(ErrorCodes.NO_RECORDS_FOUND, "No Record Found");
	}

	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Authorization User Group Details", response = UserGroup.class)
	@Path("/customers/{customerid}/groups/authorisation")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid specification"),
			@ApiResponse(code = 404, message = "Request not found") })
	public Response getUserGroupDetails(
			@ApiParam(value = "Customer ID", required = true) @PathParam("customerid") String customerId) {

		LOGGER.debug("AuthorizationResource getUserGroupDetails() STARTs");

		UserGroup userGroup;

		if (configuration.isTesting()) {
			try {
				userGroup =  new ObjectMapper().readValue(FileUtils.getFileAsStream(Constants.AUTHORIZATION_USER_GROUP_JSON),
						UserGroup.class);
				
				if(userGroup != null){
					return Response.status(HttpStatus.OK_200).entity(userGroup)
							.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
							.header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, ACCESS_CONTROL_ALLOW_HEADERS).build();
				}
			} catch (IOException e) {
				String msg = "User Group JSON not found/has error";
				AuthorizationServiceException pe = new AuthorizationServiceException(ErrorCodes.NO_RECORDS_FOUND, msg,
						e);
				LOGGER.error(msg, pe);
				throw pe;
			}
		}
		LOGGER.debug("AuthorizationResource getUserGroupDetails() ENDs");
		throw new AuthorizationServiceException(ErrorCodes.NO_RECORDS_FOUND, "No Record Found");
	}
	
	@GET
	@Timed
	@ApiOperation(value = "Ping", notes = "Check app status.")
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value = "/ping")
	public Response ping() {
		LOGGER.debug("Ping Called");
		return Response.status(200).build();
	}

	@OPTIONS
	@Path("/customers/{customerid}/audit")
	public Response insertOrUpdateCatalogueTrackerDetails() {
		return getOptions();
	}

	private Response getOptions() {
		return Response.ok("").header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS,
						HttpHeaders.ORIGIN + ", " + HttpHeaders.CONTENT_TYPE + ", " + HttpHeaders.ACCEPT + ", "
								+ HttpHeaders.AUTHORIZATION + ", ")
				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, Boolean.TRUE)
				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS,
						"PATCH" + ", " + HttpMethod.GET + ", " + HttpMethod.POST + ", " + HttpMethod.PUT + ", "
								+ HttpMethod.DELETE + ", " + HttpMethod.OPTIONS + ", " + HttpMethod.HEAD)
				.header(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "1209600").build();
	}
}