package com.morrisons.wholesale.authorization.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupUser {

    @JsonProperty("customerid")
    private String customerid;
    @JsonProperty("groupId")
    private String groupId;
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("groupDescription")
    private String groupDescription;
    @JsonProperty("groupStatus")
    private String groupStatus;
    @JsonProperty("userList")
    private List<UserList> userList = new ArrayList<>();

    @JsonProperty("customerid")
    public String getCustomerid() {
        return customerid;
    }

    @JsonProperty("customerid")
    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    @JsonProperty("groupId")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("groupName")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("groupName")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("groupDescription")
    public String getGroupDescription() {
        return groupDescription;
    }

    @JsonProperty("groupDescription")
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    @JsonProperty("groupStatus")
    public String getGroupStatus() {
        return groupStatus;
    }

    @JsonProperty("groupStatus")
    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    @JsonProperty("userList")
    public List<UserList> getUserList() {
        return userList;
    }

    @JsonProperty("userList")
    public void setUserList(List<UserList> userList) {
        this.userList = userList;
    }

}
