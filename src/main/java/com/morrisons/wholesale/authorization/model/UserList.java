package com.morrisons.wholesale.authorization.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserList {

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userDescription")
    private String userDescription;
    @JsonProperty("userLogon")
    private String userLogon;
    @JsonProperty("adminFlag")
    private String adminFlag;
    @JsonProperty("createdDate")
    private String createdDate;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedDate")
    private String updatedDate;
    @JsonProperty("updatedBy")
    private String updatedBy;

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("userDescription")
    public String getUserDescription() {
        return userDescription;
    }

    @JsonProperty("userDescription")
    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    @JsonProperty("userLogon")
    public String getUserLogon() {
        return userLogon;
    }

    @JsonProperty("userLogon")
    public void setUserLogon(String userLogon) {
        this.userLogon = userLogon;
    }

    @JsonProperty("adminFlag")
    public String getAdminFlag() {
        return adminFlag;
    }

    @JsonProperty("adminFlag")
    public void setAdminFlag(String adminFlag) {
        this.adminFlag = adminFlag;
    }

    @JsonProperty("createdDate")
    public String getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("createdDate")
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("updatedDate")
    public String getUpdatedDate() {
        return updatedDate;
    }

    @JsonProperty("updatedDate")
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}
