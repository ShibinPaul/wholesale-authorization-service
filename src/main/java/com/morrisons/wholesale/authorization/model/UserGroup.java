package com.morrisons.wholesale.authorization.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserGroup {

	@JsonProperty("customerid")
	private String customerid;
	@JsonProperty("groupList")
	private List<GroupList> groupList = new ArrayList<>();;

	@JsonProperty("customerid")
	public String getCustomerid() {
		return customerid;
	}

	@JsonProperty("customerid")
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	@JsonProperty("groupList")
	public List<GroupList> getGroupList() {
		return groupList;
	}

	@JsonProperty("groupList")
	public void setGroupList(List<GroupList> groupList) {
		this.groupList = groupList;
	}

}
