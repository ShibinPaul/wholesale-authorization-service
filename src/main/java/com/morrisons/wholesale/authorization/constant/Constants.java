package com.morrisons.wholesale.authorization.constant;

public class Constants {
	public static final String MCOLLS = "mccolls";
	public static final long DEFAULTLIMIT = 1000000000001L;
	public static final String AUTHORIZATION_GROUP_USER_JSON = "/json/Authorization_Group_User.json";
	public static final String AUTHORIZATION_USER_GROUP_JSON = "/json/Authorization_User_Group.json";

	protected Constants() {
	}

}
