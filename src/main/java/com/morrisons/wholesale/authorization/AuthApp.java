package com.morrisons.wholesale.authorization;

import java.util.HashMap;
import java.util.Map;

import com.morrisons.wholesale.authorization.configuration.AuthorizationConfiguration;
import com.morrisons.wholesale.authorization.exception.AuthorizationServiceExceptionMapper;
import com.morrisons.wholesale.authorization.healthcheck.AuthorizationHealthCheck;
import com.morrisons.wholesale.authorization.module.AuthorizationModule;
import com.morrisons.wholesale.authorization.resources.AuthorizationResource;

import de.thomaskrille.dropwizard_template_config.TemplateConfigBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.ResourceInstaller;

public class AuthApp extends Application<AuthorizationConfiguration>{
	
	private static final String SERVER = "server";
	public static final String ENV_KEY = "xxwmm_env";
	private static final Map<String, String> envSpecificConfig = new HashMap<>();

	static {
		envSpecificConfig.put("dev", "application_DEV.yml");
		envSpecificConfig.put("local", "application_LOCAL.yml");
		envSpecificConfig.put("cit", "application_CIT.yml");
		envSpecificConfig.put("sit", "application_SIT.yml");
		envSpecificConfig.put("uat", "application_UAT.yml");
		envSpecificConfig.put("preprod", "application_PREPROD.yml");
		envSpecificConfig.put("prod", "application_PROD.yml");
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception {
		String[] appConfig;
		if (args != null && args.length > 0) {
			appConfig = args;
		} else {
			appConfig = getAppConfig();
		}
		new AuthApp().run(appConfig);
	}

	@Override
	@SuppressWarnings("all")
	public void initialize(Bootstrap<AuthorizationConfiguration> bootstrap) {
		bootstrap.setConfigurationSourceProvider(new AuthorizationResourceConfigProvider());
		bootstrap.addBundle(new SwaggerBundle<AuthorizationConfiguration>() {
			@Override
			protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(AuthorizationConfiguration configuration) {
				return configuration.getSwaggerBundleConfiguration();
			}
		});

		GuiceBundle<AuthorizationConfiguration> guiceBundle = GuiceBundle.<AuthorizationConfiguration>builder()
				.installers(ResourceInstaller.class).modules(new AuthorizationModule()).extensions(AuthorizationResource.class).build();
		bootstrap.addBundle(guiceBundle);
		bootstrap.addBundle(new TemplateConfigBundle());
	}
	
	private static String[] getAppConfig() {
		String env = getEnvironmentVariable();
		if (env == null) {
			return new String[0];
		}
		String envSpecificFilePath = envSpecificConfig.get(env.toLowerCase());
		return new String[] { SERVER, envSpecificFilePath };
	}

	private static String getEnvironmentVariable() {

		Map<String, String> envs = System.getenv();
		String retValue = "local";
		for (Map.Entry<String, String> entryMap : envs.entrySet()) {
			String envName = entryMap.getKey();
			if (envName.equalsIgnoreCase(ENV_KEY)) {
				retValue = entryMap.getValue();
				break;
			}
		}
		return retValue;
	}

	@Override
	public void run(AuthorizationConfiguration configuration, Environment environment) throws Exception {
		environment.healthChecks().register("ConfigServiceHealthCheck", new AuthorizationHealthCheck());
		environment.jersey().register(AuthorizationResource.class);
		environment.jersey().register(AuthorizationServiceExceptionMapper.class);
	}
}
