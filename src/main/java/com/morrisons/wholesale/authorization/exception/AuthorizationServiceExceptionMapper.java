package com.morrisons.wholesale.authorization.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.HttpHeaders;

public class AuthorizationServiceExceptionMapper implements ExceptionMapper<AuthorizationServiceException> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationServiceExceptionMapper.class);

	public static final int CATALOGUE_TRACKER_API_V1_API_INDEX = 33;
	public static final int HTTP_RESPONSE_CODE = 404;
	private static final String ACCESSCONTROLALLOWHEADER = "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin";

	public AuthorizationServiceExceptionMapper() {
		LOGGER.info("AuthorizationServiceExceptionMapper called");
	}

	@Override
	public Response toResponse(AuthorizationServiceException exception) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setHttpResponseCode(HTTP_RESPONSE_CODE);
		String responseErrorCode = errorResponse.getHttpResponseCode() + "." + CATALOGUE_TRACKER_API_V1_API_INDEX + "."
				+ exception.getErrorCode();
		errorResponse.setErrorCode(responseErrorCode);
		errorResponse.setErrorMessage(exception.getMessage());
		errorResponse.setErrorMoreInfo("http://developer.morrisons.com/");

		return Response.status(errorResponse.getHttpResponseCode()).entity(errorResponse)
				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, ACCESSCONTROLALLOWHEADER)
				.type(MediaType.APPLICATION_JSON).build();

	}
}
