package com.morrisons.wholesale.authorization.exception;

/**
 * The Class ErrorResponse.
 */
public class ErrorResponse {

	private int httpResponseCode;
	private String errorCode;
	private String errorMessage;
	private String errorMoreInfo;

	public int getHttpResponseCode() {
		return httpResponseCode;
	}

	public void setHttpResponseCode(int httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMoreInfo() {
		return errorMoreInfo;
	}

	public void setErrorMoreInfo(String errorMoreInfo) {
		this.errorMoreInfo = errorMoreInfo;
	}
}
