package com.morrisons.wholesale.authorization.exception;

public class AuthorizationServiceException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9048962894147389848L;

	/** The error code. */
	private final int errorCode;

	/**
	 * Instantiates a new AuthorizationServiceException.
	 *
	 * @param throwable
	 *            the throwable
	 */
	public AuthorizationServiceException(Throwable throwable) {
		super(throwable);
		this.errorCode = 0;
	}

	/**
	 * Instantiates a new AuthorizationServiceException.
	 *
	 * @param errorCode
	 *            the error code
	 */
	public AuthorizationServiceException(int errorCode) {
		this(errorCode, "Error while processing the request", null);
	}

	/**
	 * Instantiates a new AuthorizationServiceException.
	 *
	 * @param errorCode
	 *            the error code
	 * @param message
	 *            the message
	 */
	public AuthorizationServiceException(int errorCode, String message) {
		this(errorCode, message, null);
	}

	/**
	 * Instantiates a new AuthorizationServiceException.
	 *
	 * @param errorCode
	 *            the error code
	 * @param message
	 *            the message
	 * @param throwable
	 *            the throwable
	 */
	public AuthorizationServiceException(int errorCode, String message, Throwable throwable) {
		super(message, throwable);
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new AuthorizationServiceException.
	 */
	public AuthorizationServiceException() {
		this(500);
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public int getErrorCode() {
		return errorCode;
	}

}
