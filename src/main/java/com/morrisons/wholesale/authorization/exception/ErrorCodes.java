package com.morrisons.wholesale.authorization.exception;

public class ErrorCodes {

	public static final Integer AN_UNEXPECTED_ERROR_HAS_OCCURRED = 0;
	public static final Integer NO_RECORDS_FOUND = 401;
	public static final int FAILED_TO_COPY_DATA = 402;
	public static final int NO_RECORDS_UPDATED_OR_INSERTED = 403;
	public static final int EMPTY_JSON = 404;
	public static final int RDS_READ_EXCEPTION = 405;
	public static final int RDS_WRITE_EXCEPTION = 406;
	public static final int ENV_NOT_FOUND = 407;
	public static final int START_OR_LIMIT_IS_BLANK = 408;
	public static final int CUSTOMER_IS_BLANK = 409;

	protected ErrorCodes() {

	}

}
