package com.morrisons.wholesale.authorization.configuration;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class AuthorizationConfiguration extends Configuration {

	/** The version. */
	@NotEmpty
	private String version;

	@NotEmpty
	private String limit;

	@JsonProperty("isTesting")
	private boolean isTesting;

	@JsonProperty("rdsConfiguration")
	@NotNull
	private DataSourceFactory database = new DataSourceFactory();

	/** The swagger bundle configuration. */
	@JsonProperty("swagger")
	private SwaggerBundleConfiguration swaggerBundleConfiguration;

	public DataSourceFactory getDatabase() {
		return database;
	}

	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}

	public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
		return swaggerBundleConfiguration;
	}

	public void setSwaggerBundleConfiguration(SwaggerBundleConfiguration swaggerBundleConfiguration) {
		this.swaggerBundleConfiguration = swaggerBundleConfiguration;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public boolean isTesting() {
		return isTesting;
	}

	public void setTesting(boolean isTesting) {
		this.isTesting = isTesting;
	}

}
