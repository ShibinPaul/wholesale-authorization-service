package com.morrisons.wholesale.authorization.utils;

import java.io.InputStream;

public class FileUtils {

	protected FileUtils() {
	}

	public static InputStream getFileAsStream(String aFileName){

		return FileUtils.class.getResourceAsStream(aFileName);
	}

}